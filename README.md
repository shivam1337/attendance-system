111608072 Shivam Poonamchand Marmat (marmat.shivam99@gmail.com)</br>
Team lead</br>
111608073 Ashitosh Tukaram Pawar</br>
Developer</br>

Project: Attendance using face recognition</br>
Technology used: opencv, congitive_face, dlib, sqlite db, openpyxl, python</br>

About project:</br>
    This project is built to solve the problem of time wasted to take attendance</br>
    in classroom everyday by replacing with capturing image of class and</br>
    uploading it to software to automatically record attendance.</br>

Requirements:</br>
    This are same as technologies mentioned above. Some are version specific</br>
    they are mentioned below:</br>
        1. openpyxl-2.4.9</br>
        2. python-2.7</br>
        3. sqlite3</br>
    API key for cognitive_face api is obtained from Microsoft Azure portal</br>
    under free tier subscription which can be further upgraded based on use.</br>