import gi
from random import randint
gi.require_version("Gtk","3.0")
from gi.repository import Gtk, Gdk
import os

window = Gtk.Window(title = "Attendance System")
window.set_size_request(300, 300)
window.connect("destroy", Gtk.main_quit)

filepath = "image.png"

def add_user(widget):
	os.system("python add_student.py")
	userid  = input("Enter recently added student mis")
	os.system("python create_person.py user" + int(userid[-2:]))
	os.system("python add_person_faces.py user" + int(userid[-2:]))
	os.system("python identify.py")
	os.system("python spreadsheet.py")

def attendance(widget):
	global filepath
	os.system("python detect.py "+ filepath)
	os.system("python spreadsheet.py")
	os.system("python identify.py")
	
def image_load(widget):
	dlg = Gtk.FileChooserDialog("Image selector", None, Gtk.FileChooserAction.OPEN,
      (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
	response = dlg.run()
	global filepath
	filepath = dlg.get_filename()
	dlg.destroy()
	print(filepath)

button = Gtk.Button("Add Student")
button.connect("clicked", add_user)
button.set_size_request(100, 50)

button3 = Gtk.Button("Select Image")
button3.connect("clicked", image_load)
button3.set_size_request(100, 50)

button2 = Gtk.Button("Attendance")
button2.connect("clicked", attendance)
button2.set_size_request(100, 50)

fixed = Gtk.Fixed()
fixed.put(button, 50,30)
fixed.put(button2, 50,170)
fixed.put(button3, 50,100)

window.add(fixed)
window.show_all()
Gtk.main()
		
